//
//  DriverDetailCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 05/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class DriverDetailCell: UITableViewCell {

    @IBOutlet var driverImageView: IconView!
    @IBOutlet var driverNameLabel: UILabel!
    @IBOutlet var ratingView: Rating!
    @IBOutlet var detailLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func bindDataWithCell(_ ride:Ride){
        driverNameLabel.text = ride.driver?.firstName
        var detailStr = ""
        let date = ride.date
        if date != nil{
            detailStr = getDateWithDay(date!,timeFlag: true)
        }
        let startMatchPoint = ride.start
        if startMatchPoint != nil{
            detailStr = "\(detailStr), von \(startMatchPoint!.info)"
        }
        let destinationMatchPoint = ride.destination
        if startMatchPoint != nil{
            detailStr = "\(detailStr) nach \(destinationMatchPoint!.info)"
        }
        detailLabel.text = detailStr
        let driver = ride.driver
        if driver != nil{
         HttpRequest.sharedRequest.image(driver!.photo, useCache: true) { image in
                self.driverImageView.image = image
            }
          ratingView.setCount(ride.driver!.score)
        }
    }
    func bindCellWithBlankData(){
        driverNameLabel.text = ""
        detailLabel.text = ""
        ratingView.setCount(5)
    }
}
