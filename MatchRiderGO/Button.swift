
import UIKit

class Button: UIButton {
  
  fileprivate var colorSet = false
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    layer.cornerRadius = 4
  }
  
  override var backgroundColor: UIColor? {
    
    get {
      return super.backgroundColor
    }
    
    set(color) {
      guard !colorSet else {
        return
      }
      super.backgroundColor = color
      colorSet = true
    }
    
  }
  
}
