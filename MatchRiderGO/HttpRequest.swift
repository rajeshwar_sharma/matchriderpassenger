
import UIKit
import Foundation
import Stripe
import CoreLocation
import Firebase
private let matchriderHost = "https://api.matchrider.de"

class HttpRequest {
    struct Endpoint {
        static let Register = "/api/register/registerUser"
        static let Login = "/api/Login/ValidateUser"
        static let AvailableRoutes = "/api/2.0/PreConfiguredRoute/GetAvailableRoutes"
        static let BookedRides = "/api/2.0/Rides/GetBookedRides"
        static let AvailableRides = "/api/2.0/PreConfiguredRoute/GetAvailableRides"
        static let BookRide = "/api/Rides/BookRide"
        static let CancelRide = "/api/Rides/CancelRide"
        static let AccountDetails = "/api/2.0/Profile/GetAccountDetails"
        static let UpdateAccountDetails = "/api/Profile/UpdateAccountDetails"
        static let CreateBankAccount = "/api/Stripe/CreateCustomer"
        static let ResetEmail = "/api/PasswordRecovery/RecoveryMail"
        static let DriverPosition = "/api/2.0/PreConfiguredRoute/GetDriverLocation"
        static let FaceBook = "/api/Login/ValidateFBUser"
        static let BankAccount =  "/api/2.0/Stripe/GetCustomerAccount"
        static let Credit = "/api/2.0/OfferAccount/GetAccountCredit"
        static let Redeem = "/api/OfferAccount/RedeemOfferCode"
        static let DeleteBankAccount = "/api/Stripe/RemoveCustomerAccount"
        static let GetThreadsByPassenger = "/api/2.0/AppMessage/GetThreadsByPassenger"
        static let GetMessagesByThread = "/api/2.0/AppMessage/GetMessagesByThread"
        static let SendMessage = "/api/FCMMessage/SendMessage"
        static let UpdateDevice = "/api/FCMMessage/UpdateDevice"
        static let getRideDetails = "/api/2.0/Rides/GetRideDetails"
        static let getUnreadThreadCount = "/api/2.0/AppMessage/GetUnreadThreadCount"
        static let updateMessageViewDate = "/api/FCMMessage/UpdateMessageViewDate"
        static let getServicedCities = "/api/2.0/PreConfiguredRoute/GetServicedCities"
        static let getAvailableRoutesByCity = "/api/2.0/PreConfiguredRoute/GetAvailableRoutesByCity"
        static let getNotRatedRideCount = "/api/2.0/AppMessage/GetNotRatedRideCount"
        static let getTotalNotificationsCount = "/api/2.0/AppMessage/GetTotalNotificationsCount"
        static let getRidesNotRated = "/api/2.0/Rides/GetRidesNotRated"
        static let updateRideRating = "/api/PreConfiguredRoute/UpdateRideRating"
        static let preconfiguredDriverSignUpDetails = "/api/PreconfiguredDriver/PreconfiguredDriverSignUpDetails"
        static let getMessageThreadId = "/api/AppMessage/GetMessageThreadId"
        static let getPassengeriOSAppVersion = "/api/2.0/Version/GetPassengeriOSAppVersion"
        static let getUserTokenForAdmin = "/api/Login/GetUserTokenForAdmin"
        
        
    }

  static let sharedRequest = HttpRequest()
  
  fileprivate var cachedImages: [String: UIImage] = [:]

  func imagesWithURL(_ url: String) -> UIImage? {
    return cachedImages[url]
  }
  
    func register(_ firstname: String, lastname: String, email: String, password: String, callback: @escaping (_ success: Bool,_ error: String?) -> Void) {
    let params = [
      "firstname": firstname,
      "lastname": lastname,
      "gender": "M",
      "email": email,
      "password": password
    ]
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.Register, bodyParams: params as [String : AnyObject]?) { payload, error in
      //callback(payload != nil)
        guard let _ = payload else {
            callback(false,error?["error"].string)
            return
        }
        callback(true,nil)
    }
  }
  
  func login(_ email: String, password: String, callback: @escaping (_ token: Token?) -> Void) {
    let params = [
      "email": email,
      "password": password
    ]
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.Login, bodyParams: params as [String : AnyObject]?) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      let token = Token(json: payload.first!)
      callback(token)
    }
  }
    func updateDevice(_ token: Token, deviceId: String, callback: @escaping (_ success : Bool?) -> Void) {
        let params = [
            "deviceId": deviceId
        ]
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.UpdateDevice,headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
            guard payload != nil else {
                callback(false)
                return
            }
            
            callback(true)
        }
    }

    func sendMessage(_ token: Token, receiverPersonId: Int, rideId: Int,message: String,index:Int, callback: @escaping (_ chats : [ChatModel]?,_ index:Int) -> Void) {
        let receiverPersonIdStr = "\(receiverPersonId)"
        let rideIdStr = "\(rideId)"
        
        let params = [
            "receiverPersonId": receiverPersonIdStr,
            "rideId": rideIdStr,
            "message": message
        ]
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.SendMessage,headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
            guard let payload = payload else {
                callback(nil,index)
                return
            }
            let chat = payload.map { json in
                return ChatModel(json: json)
            }
            callback(chat,index)
        }
    }
    
    
    func getMessageThreadId(_ token: Token,receiverPersonIds: [String], callback: @escaping (_ messageThreadId : String?) -> Void) {
        let params = [
            "receiverPersonIds": receiverPersonIds
        ]
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.getMessageThreadId,headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            var messageThreadId = ""
            _ = payload.map { json in
                messageThreadId = json.string!
            }
            print(messageThreadId)
            callback(messageThreadId)
        }
    }
    
   
    func getServicedCitiesList(_ token: Token, callback: @escaping (_ threads: [CityModel]?) -> Void) {
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.getServicedCities, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            
            let city = payload.map { json in
                return CityModel(json: json)
            }
            callback(city)
            
        }
    }

    func getAvailableRoutesByCity(_ token: Token,cityId:Int, callback: @escaping (_ routes: [Route]?) -> Void ) {
        let id = "\(cityId)"
        let components = [
            Endpoint.getAvailableRoutesByCity,
            id
        ]
        let endpoint = NSString.path(withComponents: components)
        JSONRequest(matchriderHost, method: .Get, endPoint: endpoint, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            let routes = payload.map { json in
                return Route(json: json)
            }
            callback(routes)
        }
    }
    
    
    func getPassengeriOSAppVersion(_ token: Token, callback: @escaping (_ version: String?) -> Void ) {
        
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.getPassengeriOSAppVersion, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            var versionStr = ""
            _ = payload.map { json in
                versionStr = json.string!
            }
          //let version =  NSString(string: versionStr).floatValue
            callback(versionStr)
        }
    }


    func getThreadsByPerson(_ token: Token, callback: @escaping (_ threads: [ThreadsModel]?) -> Void) {
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.GetThreadsByPassenger, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            
            let threads = payload.map { json in
                return ThreadsModel(json: json)
            }
            callback(threads)
            
        }
    }

    func getMessagesByThread(_ token: Token,threadId: String, callback: @escaping (_ chats: [ChatModel]?) -> Void) {
        
        let components = [
            Endpoint.GetMessagesByThread,
            threadId
            
        ]
        let endpoint = NSString.path(withComponents: components)
        JSONRequest(matchriderHost, method: .Get, endPoint: endpoint, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            
            let chats = payload.map { json in
                return ChatModel(json: json)
            }
            callback(chats)

        }
    }
    func getUnreadThreadCount(_ token: Token, callback: @escaping (_ count: Int) -> Void) {
        
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.getUnreadThreadCount, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(0)
                return
            }
            var count = 0
            _ = payload.map { json in
                count = json.int!
            }
            callback(count)
            
        }
    }

    func getNotRatedRideCount(_ token: Token, callback: @escaping (_ count: Int) -> Void) {
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.getNotRatedRideCount, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(0)
                return
            }
            var count = 0
            _ = payload.map { json in
                count = json.int!
            }
            callback(count)
        }
    }
    
    func getTotalNotificationsCount(_ token: Token, callback: @escaping (_ count: Int) -> Void) {
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.getTotalNotificationsCount, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(0)
                return
            }
            var count = 0
            _ = payload.map { json in
                count = json.int!
            }
            callback(count)
        }
    }
    
   
    func getRidesNotRated(_ token: Token, callback: @escaping (_ rides: [Ride]?) -> Void) {
        JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.getRidesNotRated, headerParams: token.params) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            let rides = payload.map { json in
                return Ride(json: json)
            }
            callback(rides)
        }
    }
    
    func updateRideRating(_ token: Token,passBookId:Int,rideRating:Int, callback: @escaping (_ success: Bool) -> Void) {
        let params = [
            "passBookId": passBookId,
            "rideRating": rideRating
        ]
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.updateRideRating,headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
            guard payload != nil else {
                callback(false)
                return
            }
            callback(true)
        }
    }

    
    func updateMessageViewDate(_ token: Token,messageThreadId:String, callback: @escaping (_ success: Bool) -> Void) {
        let params = [
            "MessageThreadId": messageThreadId
        ]
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.updateMessageViewDate,headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
            guard payload != nil else {
                callback(false)
                return
            }
          callback(true)
        }
    }
 
    func getRideDetails(_ token: Token,driverRequestId: Int, callback: @escaping (_ chats: Ride?) -> Void) {
         let driverRequestIdStr = "\(driverRequestId)"
        
        let components = [
            Endpoint.getRideDetails,
            driverRequestIdStr
            
        ]
        let endpoint = NSString.path(withComponents: components)
         RawRequest(matchriderHost, method: .Get, endPoint: endpoint, headerParams: token.params) { payload in
            guard let data = payload else {
                callback(nil)
                return
                
            }
            let json = JSON(data: data)
            let payload = json["Payload"]
            switch json["Status"].string! {
            case HttpRequest.Status.Success:
                if (json["Payload"].null != nil) {
                    callback(nil)
                    return
                }
             let rideDetail =   Ride(json: payload)
                callback(rideDetail)
            case HttpRequest.Status.Failure:
                callback(nil)
            default:
                callback(nil)
            }
        }

     }

    
  func facebookLogin(_ facebookToken: String, callback: @escaping (_ token: Token?) -> Void) {
    let params = [
      "tokenId": facebookToken
    ]
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.FaceBook, bodyParams: params as [String : AnyObject]?) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      let token = Token(json: payload.first!)
      callback(token)
    }
  }
  
  func createBankAccount(_ token: Token, sourceParams: STPSourceParams, callback: @escaping (_ success: Bool) -> Void) {
    
    STPAPIClient.shared().createSource(with: sourceParams) { (source, error) in
        if let s = source, s.flow == .none && s.status == .chargeable {
            
            let params = [
                "tokenId": s.stripeID
            ]
            print(s.stripeID)
            JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.CreateBankAccount, headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
                guard payload != nil else {
                    callback(false)
                    return
                }
                callback(true)
            }
        }
        else{
            callback(false)
            return

        }
    }
    
}
  
  func deleteBankAccount(_ token: Token, callback: @escaping (_ success: Bool) -> Void) {
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.DeleteBankAccount, headerParams: token.params) { payload, error in
      guard payload != nil else {
        callback(false)
        return
      }
      callback(true)
    }
  }
  
  func bookedRides(_ token: Token, callback: @escaping (_ rides: [Ride]?) -> Void) {
    JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.BookedRides, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      let rides = payload.map { json in
        return Ride(json: json)
      }
      callback(rides)
    }
  }
  
   
    
   func availableRoutes(_ token: Token,currentLocation:CLLocation?, callback: @escaping (_ routes: [Route]?) -> Void ) {
        var endPointStr = Endpoint.AvailableRoutes
        if let currentLocation = currentLocation{
        endPointStr = "\(endPointStr)/\(currentLocation.coordinate.latitude)/\(currentLocation.coordinate.longitude)"
        }
    
    JSONRequest(matchriderHost, method: .Get, endPoint: endPointStr, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      let routes = payload.map { json in
        return Route(json: json)
      }
      callback(routes)
    }
  }
  
  func availableRides(_ token: Token, routeId: Int, startMatchPointId: Int, destinationMatchPointId: Int, callback: @escaping (_ rides: [Ride]?) -> Void ) {
    let components = [
      Endpoint.AvailableRides,
      String(routeId),
      String(startMatchPointId),
      String(destinationMatchPointId)
    ]
    let endpoint = NSString.path(withComponents: components)
    JSONRequest(matchriderHost, method: .Get, endPoint: endpoint, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      let rides = payload.map { json in
        return Ride(json: json)
      }
      callback(rides)
    }
  }
  
  func passwordReset(_ email: String) {

    let components = [
        Endpoint.ResetEmail,
        "\(email)"
        ]

    
    let endpoint = NSString.path(withComponents: components)
    RawRequest(matchriderHost, method: .Get, endPoint: endpoint) { payload in }
  }
  
    func bookRide(_ token: Token, ride: Ride, callback: @escaping (_ error: ResponseError?,_ errorMsg: String?) -> Void) {
    
    
    let params = [
      "rideId": String(ride.id),
      "startMatchPointId": "\(ride.start!.id)",
      "destinationMatchPointId": "\(ride.destination!.id)"
    ]
    
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.BookRide, headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
      if payload != nil {
        callback(nil,nil)
        return
      }
      if error?["reason"].string == "missing_bank_account" {
        callback(.MissingBankAccount,error?["error"].string)
      } else {
        callback(.InvalidResponse,error?["error"].string)
      }
    }
  }
  
    func cancelRide(_ token: Token, rideId: Int, callback: @escaping (_ success: Bool, _ error: String?) -> Void) {
    let params = [
      "rideId": String(rideId)
    ]
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.CancelRide, headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
      //callback(payload != nil)
        guard let _ = payload else {
            callback(false,error?["error"].string)
            return
        }
        callback(true,nil)
    }
  }
  
  func driverLocation(_ token: Token, rideId: Int, callback: @escaping (_ location: Location?) -> Void) {
    let components = [
      Endpoint.DriverPosition,
      String(rideId)
       // String(75199)
    ]
    let endpoint = NSString.path(withComponents: components)
    JSONRequest(matchriderHost, method: .Get, endPoint: endpoint, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      guard let first = payload.first else {
        callback(nil)
        return
      }
      let location = Location(json: first)
      callback(location)
    }
  }
  
  func accountDetails(_ token: Token, callback: @escaping (_ user: User?) -> Void) {
    JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.AccountDetails, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      guard let first = payload.first else {
        callback(nil)
        return
      }
        
      let user = User(json: first)
      callback(user)
    }
  }
  //rajeshwar
    func getUserTokenForAdmin(_ token: Token,userId:String? = nil,email:String? = nil, callback: @escaping (_ token: Token?,_ error: String?) -> Void) {
        
        var key = ""
        var value = ""
        
        if let userId = userId{
           key = "Id"
           value = userId
        }
        else if let email = email{
            key = "Email"
            value = email
        }
        
        let params = [
            key: value
        ]
        
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.getUserTokenForAdmin, headerParams: token.params, bodyParams: params as [String : AnyObject]) { payload, error in
            guard let payload = payload else {
                callback(nil,error?["error"].string)
                return
            }
           
            let token = Token(json: payload.first!)
            callback(token,nil)
        }
    }

    
    
  func bankAccount(_ token: Token, callback: @escaping (_ account: BankAccount?) -> Void) {
    JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.BankAccount, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      guard let first = payload.first else {
        callback(nil)
        return
      }
      let account = BankAccount(json: first)
      callback(account)
    }
  }
  
  func credit(_ token: Token, callback: @escaping (_ credit: Int?) -> Void) {
    JSONRequest(matchriderHost, method: .Get, endPoint: Endpoint.Credit, headerParams: token.params) { payload, error in
      guard let payload = payload else {
        callback(nil)
        return
      }
      guard let first = payload.first else {
        callback(nil)
        return
      }
      print(first)
      let credit = first["creditAmount"].int!
      callback(credit)
    }
  }
    func preconfiguredDriverSignUpDetails(_ token: Token, preConfiguredRouteId: Int,startLocationId: Int,destLocationId: Int, callback: @escaping (_ driverSignUp: [DriverSignUpModel]?) -> Void) {
        let params = ["preConfiguredRouteId": preConfiguredRouteId,
                      "startLocationId": startLocationId,
                      "destLocationId": destLocationId
            ]
        JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.preconfiguredDriverSignUpDetails, headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
            guard let payload = payload else {
                callback(nil)
                return
            }
            let driver = payload.map { json in
                return DriverSignUpModel(json: json)
            }
            callback(driver)

        }
    }
  func redeem(_ token: Token, code: String, callback: @escaping (_ success: Bool) -> Void) {
    let params = ["OfferCode": code]
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.Redeem, headerParams: token.params, bodyParams: params as [String : AnyObject]?) { payload, error in
      guard let first = payload?.first else {
        callback(false)
        return
      }
      let success = first["isOfferRedeemed"].bool!
      callback(success)
    }
  }
  
  func updateAccountDetails(_ token: Token, description: String?, photo: UIImage?) {
    var base64: AnyObject = NSNull()
    if let photo = photo {
      base64 = UIImagePNGRepresentation(photo)!.base64EncodedString(options: .lineLength64Characters) as AnyObject
    }
    let params = [
      "firstname": NSNull(),
      "lastname": NSNull(),
      "description": description!,
      "photo": base64,
      "ext": "png"
    ] as [String : Any]
   
    
    JSONRequest(matchriderHost, method: .Post, endPoint: Endpoint.UpdateAccountDetails, headerParams: token.params, bodyParams: params as [String : AnyObject]?) { _ in
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfilePick"), object: nil)
    }
  }
  
  func image(_ url: String, useCache: Bool = true, callback: @escaping (_ image: UIImage?) -> Void) {
    let components = [
      matchriderHost,
      url
    ]
    let endpoint = NSString.path(withComponents: components)
    if useCache {
      if let image = cachedImages[url] {
        callback(image)
        return
      }
    }
    RawRequest(endpoint, method: .Get) { data in
      guard let data = data else {
        callback(nil)
        return
      }
      let image = UIImage(data: data)
      self.cachedImages[url] = image
      callback(image)
    }
  }
  
  struct Status {
    static let Success = "Success"
    static let Failure = "Failure"
  }
  
  enum Method: String {
    case Post = "POST"
    case Get = "GET"
  }
  
    
}

private func RawRequest(_ host: String, method: HttpRequest.Method, endPoint:String = "", headerParams: [String: String]? = nil, bodyParams: [String:AnyObject]? = nil, callback: @escaping (_ payload: Data?) -> Void) {
  guard Reachability.isConnectedToNetwork() else {
    let controller = ActionControllerNoNetwork()
    let delegate = UIApplication.shared.delegate as! AppDelegate
    delegate.mainNavigation.topViewController!.present(controller, animated: true, completion: nil)
    return
  }
  let serverAddress = host + endPoint
  let url = URL(string: serverAddress)!
    
    var request = URLRequest(url: URL(string: serverAddress)!)
    request.httpMethod = "POST"
    
    
  request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
  request.addValue("application/json; charset=UTF-8", forHTTPHeaderField: "Accept")
  if let headerParams = headerParams {
    for (key, value) in headerParams {
      request.addValue(value, forHTTPHeaderField: key)
    }
  }
  if let bodyParams = bodyParams {
    let data = try! JSONSerialization.data(withJSONObject: bodyParams, options: [])
    let dataString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
    request.httpBody = dataString!.data(using: String.Encoding.utf8.rawValue)
  }
  request.httpMethod = method.rawValue
  let config = URLSessionConfiguration.default
  let queue = OperationQueue.main
  let session = URLSession(configuration: config, delegate: nil, delegateQueue: queue)
  UIApplication.shared.isNetworkActivityIndicatorVisible = true
  
    let task = session.dataTask(with: request, completionHandler: { data, response, error in
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
    if let httpResponse = response as? HTTPURLResponse {
        if httpResponse.statusCode == 401{
            Store.sharedStore.logout()
            return
        }
    }
        
    guard error == nil else {
      callback(nil)
      return
    }
    guard let data = data else {
      callback(nil)
      return
    }
    guard response != nil else {
      callback(nil)
      return
    }
    callback(data)
  })
    
  task.resume()
}
func logout() {
    Store.sharedStore.reset()
    FBSDKLoginManager().logOut()
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginController")
    delegate.mainNavigation.setViewControllers([controller], animated: false)
    delegate.mainNavigation.dismiss(animated: true, completion: nil)
}

private func JSONRequest(_ host: String, method: HttpRequest.Method, endPoint:String, headerParams: [String: String]? = nil, bodyParams: [String:AnyObject]? = nil, bodyString: String? = nil, callback: @escaping (_ payload: [JSON]?, _ error: JSON?) -> Void) {
  RawRequest(host, method: method, endPoint: endPoint, headerParams: headerParams, bodyParams: bodyParams) { payload in
    guard let data = payload else {
      callback(nil, nil)
      return
    }
    let json = JSON(data: data)
    print(json)
    let payload = json["Payload"].array!
    switch json["Status"].string! {
    case HttpRequest.Status.Success:
      callback(payload, nil)
    case HttpRequest.Status.Failure:
      callback(nil, payload.first)
    default:
      callback(nil, nil)
    }
  }
}

import SystemConfiguration

open class Reachability {
  
  class func isConnectedToNetwork() -> Bool {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
  
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
        $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
            SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
        }
    }
    
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
      return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
  }
  
}

func registerFCMDeviceTokenToServer(token: Token!){
    if let deviceTokenId = FIRInstanceID.instanceID().token() {
        HttpRequest.sharedRequest.updateDevice(token, deviceId: deviceTokenId, callback: { success in
            
        })
    }
}

enum ResponseError: String {
  
  case InvalidResponse = "invalid_response"
  case MissingBankAccount = "missing_bank_account"
  
}

func getStringValue(json: JSON)->String!{
    let value =  json != JSON.null ? json.string : ""
    return value
}
func getIntValue(json: JSON)->Int!{
    let value =  json != JSON.null ? json.int : 0
    return value
}
func getDoubleValue(json: JSON)->Double!{
    let value =  json != JSON.null ? json.double : 0
    return value
}
