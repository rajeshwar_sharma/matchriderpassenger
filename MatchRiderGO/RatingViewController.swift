//
//  RatingViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 19/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit
import Cosmos
class RatingViewController: UIViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var userImageView: IconView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var halfCosmosView: CosmosView!
    var ride: Ride!
    let thanksLabel = UILabel()
    
     override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Bewertung abgeben"
        thanksLabel.font = UIFont.systemFont(ofSize: 30, weight: UIFontWeightBold)
        thanksLabel.frame = CGRect(x: 0, y: 120, width: WIDTH, height: 40)
        thanksLabel.textColor = hexStringToUIColor(hex: "3C3D3C")
        thanksLabel.isHidden = true
        thanksLabel.textAlignment = .center
        thanksLabel.text = "Danke!"
        self.view.addSubview(thanksLabel)
        halfCosmosView.settings.fillMode = .full
        halfCosmosView.didFinishTouchingCosmos = { rating in
            print(Float(rating))
        }
        halfCosmosView.didTouchCosmos = { rating in
            print(Float(rating))
            self.halfCosmosView.rating = rating
        }
        HttpRequest.sharedRequest.image(ride.driver!.photo, useCache: true) { image in
            self.userImageView.image = image
        }
        descriptionLabel.text = "Wie war deine Fahrt mit \(ride.driver!.firstName)?"
    }
    
    fileprivate func updateRideRating(_ token: Token,rideRating:Int) {
        self.view.showLoader()
        HttpRequest.sharedRequest.updateRideRating(token, passBookId: ride.passBookId, rideRating: rideRating) { success in
            self.view.hideLoader()
            if success{
                self.contentView.isHidden = true
                self.thanksLabel.isHidden = false
                self.navigateToBackViewController()
            }
        }
    }
    
    func navigateToBackViewController(){
        let when = DispatchTime.now() + 1.1 // change 1.1 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

  @IBAction func sendButtonAction(_ sender: AnyObject) {
        if Int(halfCosmosView.rating) > 0 {
         updateRideRating(Store.sharedStore.token!, rideRating: Int(halfCosmosView.rating))
        }
    }
 }
