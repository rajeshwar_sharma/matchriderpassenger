
import UIKit

class DetailsController: UITableViewController {
  
  var ride: Ride!
  
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var startLabel: UILabel!
  @IBOutlet weak var destinationLabel: UILabel!
  @IBOutlet weak var depatureLabel: UILabel!
  @IBOutlet weak var arrivalLabel: UILabel!
  @IBOutlet weak var durationLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var distanceLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var ratingView: Rating!
  @IBOutlet weak var driverView: IconView!
  @IBOutlet weak var modelLabel: UILabel!
  @IBOutlet weak var licenceLabel: UILabel!
  @IBOutlet weak var colorField: UIView!
  @IBOutlet weak var carView: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let duration = (ride.destination?.delay)! - (ride.start?.delay)!
    let arrivalDate = DateWithOffset(ride.date!, minutes: duration)
    //title = DateToString(ride.date!, dateStyle: .short)
    title = getDateWithDay(ride.date!,timeFlag: false)
    dateLabel.text = getDateWithDay(ride.date!,timeFlag: false)
    startLabel.text = ride.start?.info
    destinationLabel.text = ride.destination?.info
    depatureLabel.text = DateToString(ride.date!, timeStyle: .short)
    arrivalLabel.text = DateToString(arrivalDate, timeStyle: .short)
    durationLabel.text = "\(duration) Minuten"
    priceLabel.text = CentsToString(ride.price)
    distanceLabel.text = DistanceToString(ride.distance)
    nameLabel.text = ride.driver?.firstName
    ratingView.setCount((ride.driver?.score)!)
    modelLabel.text = ride.car?.model
    licenceLabel.text = ride.car?.license
    colorField.backgroundColor = ride.car?.color
    if let photo = HttpRequest.sharedRequest.imagesWithURL((ride.driver?.photo)!) {
      driverView.image = photo
    }
    let path = ride.car?.photo
    HttpRequest.sharedRequest.image(path!) { image in
      self.carView.image = image
    }
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.isToolbarHidden = true
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let target = segue.destination as! RouteController
    target.ride = ride
  }
  
}
