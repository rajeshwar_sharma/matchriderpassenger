//
//  String+Utility.swift
//  MatchRiderDriver
//
//  Created by daffolapmac on 25/01/17.
//  Copyright © 2017 daffolapmac. All rights reserved.
//

import Foundation
extension String{
    var decodeEmoji: String {
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if decodedStr != nil{
            return decodedStr as! String
        }
        return self
    }
    
    var encodeEmoji: String {
        let encodedStr = NSString(cString: self.cString(using: String.Encoding.nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue)
        return encodedStr as! String
    }
    
    var isNumber : Bool {
        get{
            return !self.isEmpty && self.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
        }
    }
}
