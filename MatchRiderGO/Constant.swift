//
//  Constant.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 13/01/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import Foundation

//let AppVersion = "1.2.0"
let AppVersion = "2.0"
let someNotification = "TEST"
let ENTER_VALID_EMAIL_ID   = "Geben Sie eine gültige E-Mail-ID ein"
let PASSWORD_MISMATCH = "Passwörter stimmen nicht überein"

let NoNetworkError = "Es konnte keine Verbindung mit MatchRiderGO aufgebaut werden"

