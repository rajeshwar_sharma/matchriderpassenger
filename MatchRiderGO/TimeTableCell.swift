
import UIKit

class TimeTableCell: UITableViewCell {
  
  static let identifier = "TimeTableCell"
  
  @IBOutlet var minutesLabel: UILabel!
  @IBOutlet var matchpointLabel: UILabel!
  @IBOutlet var lineImage: UIImageView!
  
  func setState(_ minutes: Int, matchpoint: String, position: Position) {
    minutesLabel.text = String(minutes)
    matchpointLabel.text = matchpoint
    lineImage.image = UIImage(named: position.rawValue)?.withRenderingMode(.alwaysTemplate)
  }
  
  enum Position: String {
    case First = "LineFirst"
    case Last = "LineLast"
    case Between = "LineBetween"
  }
  
}
