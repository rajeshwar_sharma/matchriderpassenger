import Foundation

struct User {
  let photo: String?
  let description: String?
  let personUserId: String!
    let firstName: String!
    let lastName: String!
    let isAdmin: Bool!
  
  init(json: JSON) {
//    photo = json["photo"].string
//    description = json["description"].string
//    personUserId = json["id"].string
//    firstName = json["firstName"].string != nil ? json["firstName"].string : ""
//    lastName = json["lastName"].string != nil ? json["lastName"].string : ""
//    isAdmin = json["isAdmin"].bool
    //isAdmin = true
    
    photo = getStringValue(json: json["photo"])
    description = getStringValue(json: json["description"])
    personUserId = getStringValue(json: json["id"])
    firstName = getStringValue(json: json["firstName"])
    lastName = getStringValue(json: json["lastName"])
    isAdmin = json["isAdmin"].bool

    
    //getStringValue(json: json["firstName"])
  }
  
}
