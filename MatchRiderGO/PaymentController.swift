
extension UIViewController {
  
  func bookRide(_ ride: Ride) {
    guard let token = Store.sharedStore.token else {
      return
    }
    let controller = ActionControllerBookRide {
        self.view.showLoader()
      HttpRequest.sharedRequest.bookRide(token, ride: ride) { error, errorMsg in
        guard let error = error else {
            self.view.hideLoader()
          Store.sharedStore.firstRideDone = true
          self.dismiss(animated: true, completion: nil)
          return
        }
        self.view.hideLoader()
        switch error {
        case .InvalidResponse:
          let controller = ActionControllerCommonAlert(errorMsg)
          self.present(controller, animated: true, completion: nil)
            
        case .MissingBankAccount:
          let action = ActionControllerPaymentMissing(errorMsg) {
            self.showPaymentController()
          }
          self.present(action, animated: true, completion: nil)
        }
      }
    }
    self.present(controller, animated: true, completion: nil)
    
  }
  
  func dismiss() {
    self.dismiss(animated: true, completion: nil)
  }
    func dismiss1() {
        self.dismiss(animated: true, completion: nil)
    }
  
  fileprivate func showPaymentController() {
    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "payment") as! UINavigationController
    let button = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(UIViewController.dismiss1))
    button.tintColor = UIColor.white
    controller.topViewController?.navigationItem.leftBarButtonItem = button
    self.present(controller, animated: true, completion: nil)
  }
  
}
