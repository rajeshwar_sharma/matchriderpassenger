
import UIKit
import Foundation

class LicenceController: UIViewController {
  
  @IBOutlet var webView: UIWebView!
  
  var legalPath: String!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    webView.scalesPageToFit = true
    webView.contentMode = .scaleAspectFit
    webView.loadRequest(URLRequest(url: URL(fileURLWithPath: Bundle.main.path(forResource: "AGB", ofType: "docx")!)))
  }
  
  @IBAction func cancel() {
    self.dismiss(animated: true, completion: nil)
  }

}
