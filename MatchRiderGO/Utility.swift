//
//  Utility.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 14/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation
class Utility {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
   class func getCurrentNavigationController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UINavigationController {
    
    
    let rootVC = base
    if rootVC?.presentedViewController == nil {
        if let nav = base as? UINavigationController {
            return nav
        }
    }

    if let presented = rootVC?.presentedViewController {
        if presented.isKind(of: UINavigationController.self) {
            let navigationController = presented as! UINavigationController
            return navigationController
        }
      }
    
   return  UINavigationController()
}
    
}

 func isValidEmail(email:String) -> Bool {
    let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: email)
}

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}
let userDefault = UserDefaults.standard

var updateAppCheck = false
func showAlertForNewVersionUpdate(){
    let actionSheetController: UIAlertController = UIAlertController(title: "Neues Update gefunden", message: "Um die App weiter nutzen zu können, lade dir bitte das Update herunter", preferredStyle: .alert)
    let cancel: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { action -> Void in
        exit(0)
    }
    actionSheetController.addAction(cancel)
    
    let okAction: UIAlertAction = UIAlertAction(title: "Update", style: .default) { action -> Void in
        UIApplication.shared.openURL(URL(string: "https://itunes.apple.com/de/app/matchridergo-mitfahrgelegenheit/id1118377939?mt=8")!)
    }
    actionSheetController.addAction(okAction)
    
    
    
    let navController = Utility.getCurrentNavigationController()
    navController.present(actionSheetController, animated: true, completion: nil)
}

//func showAlertView(msg: String){
//    let actionSheetController: UIAlertController = UIAlertController(title: "Token", message: msg, preferredStyle: .alert)
//    let okAction: UIAlertAction = UIAlertAction(title: "ok", style: .default) { action -> Void in
//        
//    }
//    actionSheetController.addAction(okAction)
//    let navController = Utility.getCurrentNavigationController()
//    navController.present(actionSheetController, animated: true, completion: nil)
//    
//}
