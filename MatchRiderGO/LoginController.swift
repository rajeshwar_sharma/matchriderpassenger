
import UIKit
import UserNotifications
import Firebase

class LoginController: UITableViewController {
    @IBOutlet var customTableView: UITableView!
  
  @IBOutlet weak var emailField: UITextField!
  @IBOutlet weak var passwordField: UITextField!
  @IBOutlet weak var buttonView: UIView!
  @IBOutlet var loginButton: UIButton!
  var resetField: UITextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    let facebookButton = FBSDKLoginButton()
    facebookButton.readPermissions = ["email"]
    facebookButton.loginBehavior = .web
    facebookButton.delegate = self
    facebookButton.frame = CGRect(x: 0, y: 0, width: 120, height: 30)
    buttonView.addSubview(facebookButton)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    emailField.text = Store.sharedStore.email
    navigationController?.isNavigationBarHidden = true
    navigationController?.isToolbarHidden = true
   }
   
  @IBAction func startIntro() {
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let controller = OnboardingController(nibName: "OnboardingMain", bundle: nil)
    delegate.mainNavigation.setViewControllers([controller], animated: true)
  }
  
  @IBAction func sendLogin(_ sender: AnyObject) {
    
    if NetworkReachability.isConnectedToNetwork(){
        loginUser()
    }else{
        let controller = ActionControllerCommonAlert(NoNetworkError)
        self.present(controller, animated: true, completion: nil)
    }
  }
  
    func loginUser(){
        let email = emailField.text ?? ""
        let password = passwordField.text ?? ""
        emailField.isEnabled = false
        passwordField.isEnabled = false
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.view.showLoader()
        HttpRequest.sharedRequest.login(email, password: password) { token in
            self.emailField.isEnabled = true
            self.passwordField.isEnabled = true
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            guard token != nil else {
                let alert = ActionControllerInvalidCredentials()
                self.present(alert, animated: true, completion: nil)
                self.view.hideLoader()
                return
            }
            Store.sharedStore.token = token
            Store.sharedStore.email = self.emailField.text
            self.registerFCMDeviceTokenToServer(token: token)
            self.view.hideLoader()
            self.performSegue(withIdentifier: "bookedRides", sender: nil)
        }
    }
    
  @IBAction func resetPassword() {
    let controller = ActionControllerResetPassword {
      HttpRequest.sharedRequest.passwordReset(self.resetField.text ?? "")
    }
    controller.addTextField { textField in
      self.resetField = textField
      self.resetField.delegate = self
      self.resetField.placeholder = "Email"
    }
    present(controller, animated: true, completion: nil)
  }
  
  @IBAction func undwindToLogin(_ segue: UIStoryboardSegue) {
    Store.sharedStore.reset()
  }
 
}

extension LoginController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
}

extension LoginController: FBSDKLoginButtonDelegate {
    /*!
     @abstract Sent to the delegate when the button was used to login.
     @param loginButton the sender
     @param result The results of the login
     @param error The error (if any) from the login
     */
    public func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        guard result?.token?.tokenString != nil else
        {
            self.loginButton.isEnabled = true
            return
        }
        self.buttonView.isHidden = true
        
        HttpRequest.sharedRequest.facebookLogin(result.token.tokenString) { token in
            guard token != nil else {
                let alert = ActionControllerInvalidCredentials()
                self.present(alert, animated: true, completion: nil)
                return
            }
            Store.sharedStore.token = token
            self.buttonView.isHidden = false
            self.loginButton.isEnabled = true
            self.registerFCMDeviceTokenToServer(token: token)
            self.performSegue(withIdentifier: "bookedRides", sender: nil)
        }

    }

    func registerFCMDeviceTokenToServer(token: Token!){
        if let deviceTokenId = FIRInstanceID.instanceID().token() {
            HttpRequest.sharedRequest.updateDevice(token, deviceId: deviceTokenId, callback: { success in
                print("Success")
                
            })
        }
    }
       
  func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
    
  }
  
  func loginButtonWillLogin(_ loginButton: FBSDKLoginButton!) -> Bool {
    self.loginButton.isEnabled = false
    return true
  }
  
}
