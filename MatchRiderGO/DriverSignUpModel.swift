//
//  DriverSignUpModel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 20/03/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import Foundation
class DriverSignUpModel {
    let preConfiguredRouteId: Int
    let userId: String
    let personId: Int
    let startLocationId: Int
    let destLocationId: Int
    let driverPaymentPerKm: Double
    let singleRideDriverPaymentAmount: Double
    let approximateRegularRouteMonthlyPayment: Int
    let driverSignupURL: String
    
    
    init(json: JSON) {
        preConfiguredRouteId = getIntValue(json:json["preConfiguredRouteId"])
        userId = getStringValue(json: json["userId"])
        personId = getIntValue(json:json["personId"])
        startLocationId = getIntValue(json:json["startLocationId"])
        destLocationId = getIntValue(json:json["destLocationId"])
        driverPaymentPerKm = getDoubleValue(json:json["driverPaymentPerKm"])
        singleRideDriverPaymentAmount = getDoubleValue(json:json["singleRideDriverPaymentAmount"])
        approximateRegularRouteMonthlyPayment = getIntValue(json:json["approximateRegularRouteMonthlyPayment"])
        driverSignupURL = getStringValue(json: json["driverSignupURL"])
     
    }
}
