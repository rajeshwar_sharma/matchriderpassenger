
struct BankAccount {
  
  let last4: String
  let name: String
  let address: String
  let city: String
  let zip: String
  
  init(json: JSON) {
    self.last4 = "********" + json["accountNumber"].string!
    self.name = json["name"].string!
    self.address = json["address"].string!
    self.city = json["city"].string!
    self.zip = json["postalCode"].string!
  }

}
