//
//  MessageViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 05/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class MessageViewController: UIViewController {
    @IBOutlet var textView: UITextView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var bottomHeightConstraint: NSLayoutConstraint!
    var showDriverDetail : Bool!
    var checkDriverDetail : Bool!
    var messageThreadId : String!
    var numberOfRow : Int!
    var ride: Ride!
    var chats = [ChatModel]()
    var commonModel = [Any]()
    var driverDetail = [Int:Any]()
    var rideDetailIndexPath: Int = 0
    var networkViewObj: NetworkView!
    var dataLoadingFlag = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Nachrichten"
        IQKeyboardManager.sharedManager().enable = false
        NotificationCenter.default.addObserver(self, selector: #selector(MessageViewController.fcmNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "FCMNotification"), object: nil)
        textView.delegate = self
        textView.text = "\nBitte Nachricht eingeben"
        textView.textColor = UIColor.lightGray
        tableView.isHidden = true
        checkNetworkAvailabilty()
        
        if messageThreadId != nil{
            Store.sharedStore.currentMessageThreadId = messageThreadId
        }else{
            Store.sharedStore.currentMessageThreadId = "Default"
        }
        checkDriverDetail = false
        //tableView.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "FCMNotification"), object: nil)
        IQKeyboardManager.sharedManager().enable = true
    }
    // Check network availability
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            getMessagesByThread()
            updateMessageViewDate(messageThreadId: messageThreadId)
            registerFCMDeviceTokenToServer(token: Store.sharedStore.token!)
        }
        networkViewObj?.delegate = self
    }
    //Calling api to get Message by thread
    func getMessagesByThread(){
        if ride == nil{
            self.bottomHeightConstraint.constant = -70
        }else{
            self.bottomHeightConstraint.constant = 0
        }
        if commonModel.count != 0{
            commonModel.removeAll()
            tableView.reloadData()
        }
        if messageThreadId == nil{
            if ride != nil{
                commonModel.append(NSNull())
                let dic:[String : Any] = [ "ride": ride]
                driverDetail[0] =  dic
                tableView.isHidden = false
                tableView.reloadData()
            }
            return
        }
        self.bottomHeightConstraint.constant = -70
        self.view.showLoader()
        dataLoadingFlag = true
        HttpRequest.sharedRequest.getMessagesByThread(Store.sharedStore.token!, threadId: messageThreadId!) { chats in
            guard let chats = chats else {
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            self.chats = chats
            self.configureCommonModelData()
            self.dataLoadingFlag = false
        }
    }
    
    func configureCommonModelData(){
        var index: Int = 0
        if commonModel.count != 0 {
            index = commonModel.count - 1
        }
        var addRideCheck = true
        
        for chat in chats {
            if chat.driverRequestId != 0{
                commonModel.append(NSNull())
                let dic:[String : Any] = ["ride": NSNull()]
                driverDetail[index] =  dic
                rideDetailIndexPath = index
                getRideDetail(index: index, driverRequestId: chat.driverRequestId)
                
                index += 1
            }
            if ride != nil && chat.driverRequestId == ride.id{
                addRideCheck = false
            }
            commonModel.append(chat)
            index += 1
        }
        if addRideCheck && ride != nil{
            rideDetailIndexPath = index
            commonModel.append(NSNull())
            let dic:[String : Any] = ["ride": ride]
            driverDetail[index] =  dic
            self.title = "Ich und \(ride.driver!.firstName)"
            self.bottomHeightConstraint.constant = 0
        }
        tableView.reloadData()
        tableViewScrollToBottom(animated: false)
    }
    
    func getRideDetail(index: Int, driverRequestId: Int) {
        
        HttpRequest.sharedRequest.getRideDetails(Store.sharedStore.token!, driverRequestId: driverRequestId) { ride in
            guard let ride = ride else {
                return
            }
            let dic = self.driverDetail[index]
            var dicTmp = dic as! [String: Any]
            dicTmp["ride"] = ride
            self.driverDetail[index] = dicTmp
            let indexPath = NSIndexPath(row: index, section: 0)
            self.tableView.reloadRows(at: [indexPath as IndexPath], with: .none)
            if self.rideDetailIndexPath == index{
               if ride.driver != nil{
                self.bottomHeightConstraint.constant = 0
                self.tableViewScrollToBottom(animated: false)
                self.title = "Ich und \(ride.driver!.firstName)"
                } else{
                if self.ride != nil{
                    let dic = self.driverDetail[index]
                    var dicTmp = dic as! [String: Any]
                    dicTmp["ride"] = self.ride
                    self.driverDetail[index] = dicTmp
                    self.bottomHeightConstraint.constant = 0
                    self.tableViewScrollToBottom(animated: false)
                    self.title = "Ich und \(self.ride.driver!.firstName)"
 
                }
                }
            }
        }
    }
    
    func updateMessageViewDate(messageThreadId:String?){
        if messageThreadId == nil{
            return
        }
        HttpRequest.sharedRequest.updateMessageViewDate(Store.sharedStore.token!, messageThreadId: messageThreadId!, callback: { success in
            if success{
                
            }
        })

    }
    
    func fcmNotificationReceiver(_ notification: NSNotification){
        if dataLoadingFlag{
            return
        }
        let dic = notification.userInfo as! [String:Any]
        let chatTmp = dic["chatDetail"] as! ChatModel
        let check = dic["check"] as! Bool
        ride = nil
        if check{
            self.chats.removeAll()
            self.chats.append(chatTmp)
            configureCommonModelData()
            
        }else{
            messageThreadId = chatTmp.messageThreadId
            Store.sharedStore.currentMessageThreadId = messageThreadId
            getMessagesByThread()
        }
        
    }
    
    //Use to Scroll tableview to last row
    func tableViewScrollToBottom(animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let numberOfSections = self.tableView.numberOfSections
            let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                
                let indexPath = NSIndexPath(row: numberOfRows-1, section: numberOfSections-1)
                self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.bottom, animated: animated)
                self.tableView.isHidden = false
            }
        }
    }
    
    @IBAction func sendButtonAction(_ sender: AnyObject) {
      sendMessage(chatData: nil)
    }
    //\nBitte Nachricht eingeben
    func sendMessage(chatData: ChatModel?){
        if textView.text == "\nBitte Nachricht eingeben"{
            return
        }
        
        if textView.text.trim().isEmpty && chatData == nil {
            return
        }
        var  recentChat: ChatModel!
        if chatData != nil {
           recentChat = chatData
        }
        else{
          recentChat = getRecentChatModelDummyData()
        }
        
        if commonModel.count == 0{
            return
        }
        
        commonModel.append(recentChat)
        tableView.reloadData()
        tableViewScrollToBottom(animated: false)
        let dic = driverDetail[rideDetailIndexPath]
        let dicTmp = dic as! [String: Any]
        let rideTmp = dicTmp["ride"]
        if rideTmp is NSNull {
            return
        }
        let rideDetail = rideTmp as! Ride
        if rideDetail.driver == nil{
            textView.text = ""
            return
        }
        HttpRequest.sharedRequest.sendMessage(Store.sharedStore.token!,receiverPersonId: rideDetail.driver!.id , rideId: rideDetail.id, message: recentChat.messagePosting,index: commonModel.count-1) { chats ,index in
            guard let chats = chats else {
                
                if index < self.commonModel.count{
                    let recentChatData = self.commonModel[index]
                    self.resendMessage(recentChat: recentChatData as! ChatModel)
                    self.commonModel.remove(at: index)
                    self.tableView.reloadData()
                }
                return
            }
            let chatTmp = chats.first!
            self.updateMessageViewDate(messageThreadId: chatTmp.messageThreadId)
         self.commonModel[index] = chats.first! as ChatModel
        }
        textView.text = ""
    }
    
    func resendMessage(recentChat: ChatModel){
        let alert=UIAlertController(title: "Senden der Nachricht fehlgeschlagen", message: recentChat.messagePosting, preferredStyle: UIAlertControllerStyle.alert);
        alert.addAction(UIAlertAction(title: "Abbrechen", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction) in
          print("cancel")
        }));
        alert.addAction(UIAlertAction(title: "Erneut senden", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
            self.sendMessage(chatData: recentChat)
        }));
        present(alert, animated: true, completion: nil);
    }
    
    func getRecentChatModelDummyData()-> ChatModel{
        let jsonObject: [Any]  = [
            
            [
                "PersonName": NSNull(),
                "MessagePostId": 0,
                "MessageThreadId": "",
                "MessageThreadParticipantId": "0",
                "DriverRequestId": 0,
                "MessagePosting": textView.text.trim(),
                "DateCreated": "",
                "PersonId": 0,
                "PersonPhoto": Store.sharedStore.personUserPhoto,
                "IsUnread": false,
                "PersonUserId": Store.sharedStore.personUserId
            ]
        ]
        let jsonData = try! JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        let json = JSON(data:jsonData )
        let payload = json.array!
        return ChatModel(json: payload.first!)
    }
    
    //Calculate the size of Message
    func getMessageSize(message:String) -> CGSize {
        let lblMsg = UILabel()
        lblMsg.backgroundColor = UIColor.yellow
        lblMsg.text = message
        let font = UIFont.systemFont(ofSize: 14.0)
        var desiredWidth: CGFloat = 190
        if WIDTH == 375{
            desiredWidth = 240
        }
        else if WIDTH == 414{
            desiredWidth = 270
        }
        lblMsg.font = font
        lblMsg.numberOfLines = 0;
        lblMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size: CGSize = lblMsg.sizeThatFits(CGSize(width: desiredWidth, height: .greatestFiniteMagnitude))
        return size
    }
}

extension MessageViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        return true
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
        tableViewScrollToBottom(animated: false)
        UIView.animate(withDuration: 1, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.bottomHeightConstraint.constant = 302
            }, completion: nil)
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "\nBitte Nachricht eingeben"
            textView.textColor = UIColor.lightGray
        }
        self.bottomHeightConstraint.constant = 0
    }
}

extension MessageViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commonModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let model = commonModel[indexPath.row]
        if model is NSNull{
            
            let cell:DriverDetailCell = self.tableView.dequeueReusableCell(withIdentifier: "DriverDetailCell") as! DriverDetailCell
            
            let dic = driverDetail[indexPath.row]
            let dicTmp = dic as! [String: Any]
            let rideTmp = dicTmp["ride"]
            
            if rideTmp is NSNull {
                cell.bindCellWithBlankData()
            }else{
                let rideDetail = rideTmp as! Ride
                cell.bindDataWithCell(rideDetail)
                
            }
            
            return cell
            
        }else{
            return configureMessageCell(indexPath as NSIndexPath, chat: model as! ChatModel)
        }
        
    }
    
    func configureMessageCell(_ indexPath:NSIndexPath,chat: ChatModel) -> MessageCell {
        var x : CGFloat ;
        let y : CGFloat = 13.0
        var cell : MessageCell!
        
        let lblMsg = CustomLabel()
        lblMsg.tag = 100
        lblMsg.backgroundColor = UIColor.yellow
        lblMsg.text = chat.messagePosting
        let font = UIFont.systemFont(ofSize: 14.0)
        lblMsg.font = font
        lblMsg.numberOfLines = 0;
        lblMsg.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size = getMessageSize(message: chat.messagePosting)
        let timeLabel = getTimelabel()
        var additionalWidth: CGFloat = 28
        if size.width  < 70{
            additionalWidth = 24 + 18 + 65
        }

        if chat.personUserId != Store.sharedStore.personUserId{
            cell = self.tableView.dequeueReusableCell(withIdentifier: "DriverCell") as! MessageCell
            
            x = 90
            lblMsg.backgroundColor = hexStringToUIColor(hex: "7DBE7F")
            timeLabel.textColor = UIColor.white
            lblMsg.textColor = UIColor.white
            let triangle = TriangleView(frame: CGRect(x: 0, y: 0, width: 18 , height: 26), mode: .Left)
            cell.arrowImageView.addSubview(triangle)
            
        }else{
            cell = self.tableView.dequeueReusableCell(withIdentifier: "PassengerCell") as! MessageCell
            lblMsg.backgroundColor = UIColor.white
            x = WIDTH-90
            x = x - size.width - additionalWidth
            let triangle = TriangleView(frame: CGRect(x: 0, y: 0, width: 18 , height: 26), mode: .Right)
            cell.arrowImageView.addSubview(triangle)
            lblMsg.textColor = hexStringToUIColor(hex: "858585")
            timeLabel.textColor = UIColor.gray
            
        }
        
        cell.selectionStyle = .none
        
        
        lblMsg.frame = CGRect(x: x, y: y, width: size.width + additionalWidth , height: size.height + 24 + 10 )
        if size.height < 30 {
            lblMsg.frame = CGRect(x: x, y: y, width: size.width + additionalWidth , height: 54 )
        }
        
        
        let removeLbl = cell.viewWithTag(100)
        if removeLbl != nil{
            removeLbl?.removeFromSuperview()
        }
        cell.addSubview(lblMsg)
        timeLabel.frame = CGRect(x: lblMsg.frame.width-44 - 65, y: lblMsg.frame.height-20, width: 40 + 65, height: 20)
        lblMsg.addSubview(timeLabel)
        timeLabel.text = getMessageCreatedTime(dateCreated: chat.dateCreated)
        HttpRequest.sharedRequest.image(chat.personPhoto, useCache: true) { image in
            cell.userImageView.image = image
            
        }
        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let model = commonModel[indexPath.row]
        if model is NSNull{
            return 130
        }else{
            let  chat  = model as! ChatModel
            let size = getMessageSize(message: chat.messagePosting)
            
            if size.height < 30{
                return 80
            }
            return size.height + 26 + 24
        }
    }
    
    
    func getTimelabel()-> UILabel{
        let timeLabel = UILabel()
        timeLabel.backgroundColor = UIColor.clear
        timeLabel.textAlignment = .right
        timeLabel.font = UIFont.systemFont(ofSize: 12.0)
        return timeLabel
    }
    func getMessageCreatedTime(dateCreated: String)-> String{
        let date = stringToDate(dateCreated)
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd"
        let date1 = timeFormatter.string(from: date)
        let date2 = timeFormatter.string(from: Date())
        var timeStr = ""
        
        if date1 == date2 {
            timeFormatter.dateFormat = "HH:mm"
            timeStr = timeFormatter.string(from: date)
        }else{
            timeFormatter.locale = Locale(identifier: "de_DE")
            timeFormatter.dateFormat = "d. MMM'T'yy, HH:mm"
            timeStr = timeFormatter.string(from: date)
            timeStr =  timeStr.replacingOccurrences(of: "T", with: "'")
        }
        return timeStr

    }
    func stringToDate(_ string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
        if let date = dateFormatter.date(from: string) {
            return date
        }else {
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            if let  date = dateFormatter.date(from: string) {
                return date
            }
        }
        return Date()
    }
}
extension MessageViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()

    }
}

