
import Foundation

struct Ride {
  
  let id: Int
  let driver: Driver?
  let car: Car?
  let distance: Double
  let date: Date?
  let price: Int
  let start: Matchpoint?
  let destination: Matchpoint?
  var messageThreadId: String?
  let passBookId: Int!
  
  init(json: JSON) {
    id = json["id"].int!
    distance = json["distance"].double!
    date = json["date"].string != nil ? StringToDate(json["date"].string!) : nil
    car = json["car"] != JSON.null ? Car(json: json["car"]) : nil
    driver = json["driver"] != JSON.null ? Driver(json: json["driver"]) : nil
    price = json["price"].int!
    start = json["start"] != JSON.null ? Matchpoint(json: json["start"]) : nil
    destination = json["destination"] != JSON.null ? Matchpoint(json: json["destination"]) : nil
    messageThreadId = json["messageThreadId"].string
    passBookId = json["passBookId"].int != nil ? json["passBookId"].int! : 0
  }
  
}
