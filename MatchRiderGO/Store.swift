
import Foundation

class Store {
  
  static let sharedStore = Store()
  
    
    private var _totalNotificationCount:Int! = 0
    var totalNotificationCount: Int! {
        get{
            
            return _totalNotificationCount
        }
        set{
            _totalNotificationCount = newValue
        }
        
    }

    private var _cityList = [CityModel]()
    var cityList: [CityModel]! {
        get{
            
            return _cityList
        }
        set{
            _cityList = newValue
        }
        
    }

    
    var currentMessageThreadId: String!{
        get{
            guard let personUserId = UserDefaults.standard.string(forKey: "messageThreadId") else {
                return ""
            }
            return personUserId
        }
        set(messageThreadId) {
            UserDefaults.standard.set(messageThreadId, forKey: "messageThreadId")
        }
    }

    
    var personUserPhoto: String!{
        get{
            guard let personUserId = UserDefaults.standard.string(forKey: "personUserPhoto") else {
                return ""
            }
            return personUserId
        }
        set(personUserId) {
            UserDefaults.standard.set(personUserId, forKey: "personUserPhoto")
        }
    }
    var personName: String!{
        get{
            guard let personName = UserDefaults.standard.string(forKey: "personName") else {
                return ""
            }
            return personName
        }
        set(personName) {
            UserDefaults.standard.set(personName, forKey: "personName")
        }
    }

    var isAdmin: Bool!{
        get{
            guard let isAdmin = UserDefaults.standard.object(forKey: "isAdmin") else {
                return false
            }
            return isAdmin as! Bool
        }
        set(isAdmin) {
            UserDefaults.standard.set(isAdmin, forKey: "isAdmin")
        }
    }
    
    var personUserId: String!{
        get{
            guard let personUserId = UserDefaults.standard.string(forKey: "personUserId") else {
                return ""
            }
             return personUserId
        }
        set(personUserId) {
            UserDefaults.standard.set(personUserId, forKey: "personUserId")
        }
    }
    
    var email: String? {
    
    get {
      return UserDefaults.standard.string(forKey: "MREmail")
    }
    
    set(email) {
      UserDefaults.standard.set(email, forKey: "MREmail")
    }
    
  }
  
  var token: Token? {
    
    get {
      guard let token = UserDefaults.standard.string(forKey: "MRToken") else {
        return nil
      }
      //return Token(string: "4b97de1d-2222-4ca344-c")
      return Token(string: token)
    }
    
    set(token) {
      guard let token = token else {
        return
      }
      guard token.id != "" else {
        return
      }
      UserDefaults.standard.set(token.id, forKey: "MRToken")
    }
    
  }
    
  var firstRideDone: Bool {
    
    get {
      return UserDefaults.standard.bool(forKey: "MRFirstRideDone")
    }
    
    set(done) {
      UserDefaults.standard.set(done, forKey: "MRFirstRideDone")
    }
    
  }
  
  var onboardingComplete: Bool {
    
    get {
      return UserDefaults.standard.bool(forKey: "MROnboardingComplete")
    }
    
    set(done) {
      UserDefaults.standard.set(done, forKey: "MROnboardingComplete")
    }
    
  }
    func reset() {
    UserDefaults.standard.removeObject(forKey: "MRToken")
    UserDefaults.standard.removeObject(forKey: "MRFirstRideDone")
    UserDefaults.standard.removeObject(forKey: "MRBankAccount")
  }
  func logout() {
    Store.sharedStore.reset()
    FBSDKLoginManager().logOut()
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginController")
    delegate.mainNavigation.setViewControllers([controller], animated: false)
    delegate.mainNavigation.dismiss(animated: true, completion: nil)
    }

}
