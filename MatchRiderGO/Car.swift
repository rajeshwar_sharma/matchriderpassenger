
struct Car {
  let id: Int
  let photo: String
  let model: String
  let color: UIColor
  let license: String
  
  init(json: JSON) {
    id = json["id"].int!
    photo = json["photo"].string!
    model = json["model"].string!
    color = UIColor(rgba: json["colorRGBA"].string!)
    license = json["license"].string!
  }
}