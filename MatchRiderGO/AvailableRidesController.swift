
import UIKit

class AvailableRidesController: TableController {
    
    var route: Route!
    var start: Matchpoint!
    var destination: Matchpoint!
    var date = Date()
    var availableRides: [Ride]?
    var showDriverSignUp = false
    var driverSignUp: DriverSignUpModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = RouteDirectionString(route.fromShortName, to: route.toShortName)
        showMessage("Ergebnisse werden geladen")
        driverSignUpDetails()
        requestAvailableRides(Store.sharedStore.token!)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isToolbarHidden = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableRides?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BookCell.identifier, for: indexPath) as! BookCell
        let ride = availableRides![(indexPath as NSIndexPath).row]
        let photo = HttpRequest.sharedRequest.imagesWithURL((ride.driver?.photo)!)
        cell.setStateWithDriverName((ride.driver?.firstName)!, rating: (ride.driver?.score)!, date: ride.date!, image: photo, price: ride.price)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        if showDriverSignUp{
            let tap = UITapGestureRecognizer(target: self, action:#selector(self.handleTap(sender:)))
            vw.addGestureRecognizer(tap)
            let cell = tableView.dequeueReusableCell(withIdentifier: "DriverSignUpCell") as! DriverSignUpCell
            cell.bindDataWithCell(driver: driverSignUp)
            cell.frame = CGRect(x: 0, y: 0, width: WIDTH, height: 119)
            
            let lineView = UIView()
            lineView.backgroundColor = UIColor.lightGray
            lineView.frame = CGRect(x: 0, y: 119, width: WIDTH, height: 1)
            vw.addSubview(cell)
            vw.addSubview(lineView)
            return vw
        }
        return vw
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverSignUpViewController") as! DriverSignUpViewController
        vc.driverSignUp = driverSignUp
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if showDriverSignUp{
            return 120
        }
        return 0.01
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let target = segue.destination as! DetailsController
        let row = (tableView.indexPathForSelectedRow! as NSIndexPath).row
        target.ride = availableRides![row]
    }
    
    func driverSignUpDetails(){
        HttpRequest.sharedRequest.preconfiguredDriverSignUpDetails(Store.sharedStore.token!, preConfiguredRouteId: route.id, startLocationId: start.id, destLocationId: destination.id) { (driverSignUp) in
            guard let driverSignUp = driverSignUp else {
                return
            }
            if driverSignUp.count != 0{
                let driver = driverSignUp.first
                self.showDriverSignUp = true
                self.driverSignUp = driver
                self.tableView.reloadData()
                //print(driver)
            }
        }
    }
    
    fileprivate func requestAvailableRides(_ token: Token) {
        self.view.showLoader()
        HttpRequest.sharedRequest.availableRides(token, routeId: route.id, startMatchPointId: start.id, destinationMatchPointId: destination.id) { rides in
            self.showTableView()
            self.tableView.separatorStyle = .none
            guard let rides = rides else {
                self.view.hideLoader()
                let error = ActionControllerServerConnection {
                    self.requestAvailableRides(token)
                }
                self.present(error, animated: true, completion: nil)
                return
            }
            self.view.hideLoader()
            self.availableRides = rides
            for item in self.toolbarItems ?? [] {
                item.isEnabled = true
            }
            self.tableView.reloadData()
            for ride in rides {
                let path = ride.driver?.photo
                HttpRequest.sharedRequest.image(path!) { image in
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func bookRide(_ sender: UIButton) {
        let buttonPosition = sender.convert(CGPoint.zero, to: tableView)
        guard let row = (tableView.indexPathForRow(at: buttonPosition) as NSIndexPath?)?.row else {
            return
        }
        let ride = availableRides![row]
        bookRide(ride)
        
    }
    
}
