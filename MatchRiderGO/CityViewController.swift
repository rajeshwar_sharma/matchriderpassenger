//
//  CityViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 22/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    var checkFirstTime: Bool!
    var cityModel = [CityModel]()
    var networkViewObj: NetworkView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Wähle eine Stadt"
        if Store.sharedStore.cityList.count == 0{
            getCityList()
        }else{
            cityModel = Store.sharedStore.cityList
        }
        checkNetworkAvailabilty()
        if checkFirstTime!{
            setLeftBarButton()
        }
        tableView.hideEmptyRow()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        networkViewObj?.delegate = self
    }
    
    func getCityList(){
       HttpRequest.sharedRequest.getServicedCitiesList(Store.sharedStore.token!) { cityList in
            
            guard let cityList = cityList else {
                return
            }
            self.cityModel = cityList
        Store.sharedStore.cityList = cityList
            self.tableView.reloadData()
        }
    }
    
    
    func setLeftBarButton()  {
        
        let leftBarButtonItem =  UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: self, action:#selector(CityViewController.cancelButtonAction))
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false)
        }
    
    
    func cancelButtonAction() {
        if checkFirstTime! {
            return
        }
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }

}
extension CityViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "CityCell") as! CityCell
        let city = cityModel[indexPath.row]
        cell.bindDataWithCell(city: city)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = cityModel[indexPath.row]
        userDefault.set(city.cityId, forKey: "CityId")
        
        if checkFirstTime{
            self.dismiss()
        }else{
            checkFirstTime = false
            cancelButtonAction()
        }
        
        
        }
}

extension CityViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
        if networkViewObj.isNetworkAvailable!{
           getCityList()
        }
    }
}
