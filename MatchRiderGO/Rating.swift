
import UIKit

class Rating: UIView {
  
  let label = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    addSubview(label)
    label.adjustsFontSizeToFitWidth = true
    
  }
    override func layoutSubviews() {
        label.frame = self.bounds
    }
    
  func setCount(_ count: Int) {
   
    let stars = NSMutableAttributedString(string: "★★★★★")
    let leftRange = NSMakeRange(0, count)
    let rightRange = NSMakeRange(count, stars.string.characters.count - count)
    let blueColor = MatchRiderColor
    let grayColor = UIColor(white: 140/255.0, alpha: 1)
    stars.addAttribute(NSForegroundColorAttributeName, value: blueColor, range: leftRange)
    stars.addAttribute(NSForegroundColorAttributeName, value: grayColor, range: rightRange)
    label.attributedText = stars
    label.frame = self.bounds
    
  }

}
