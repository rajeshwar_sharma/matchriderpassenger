
import Foundation
import UIKit
import MapKit

class Matchpoint: NSObject, MKAnnotation {
  
  let id: Int
  let latitude: Double
  let longitude: Double
  let name: String
  let whereToStand: String
  let info: String
  let street: String
  let postal: String
  let city: String
  let photo: String
  let delay: Int
  
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
  
  var title: String? {
    return info
  }
  
  init(json: JSON) {
    id = json["id"].int!
    latitude = json["latitude"].double!
    longitude = json["longitude"].double!
    name = json["name"].string!
    whereToStand = json["whereToStand"].string!
    info = json["description"].string!
    street = json["street"].string!
    postal = json["postalCode"].string!
    city = json["city"].string!
    photo = json["photo"].string! 
    delay = json["delay"].int! / 60
  }
  
}

class DriverLocation: NSObject, MKAnnotation{
  
  let latitude: Double
  let longitude: Double
  
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
  
  var title: String? {
    return "Fahrer"
  }
  
  init(lat: Double, long: Double) {
    latitude = lat
    longitude = long
  }
  
}
