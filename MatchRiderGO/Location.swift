
import Foundation
import UIKit
import MapKit

class Location: NSObject, MKAnnotation {
  
  let latitude: Double
  let longitude: Double
  
  var coordinate: CLLocationCoordinate2D {
    return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
  }
  
  var title: String? {
    return "Fahrer"
  }
  
  init(json: JSON) {
    latitude = json["latitude"].double!
    longitude = json["longitude"].double!
  }
  
}
