//
//  UserDetailCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 31/05/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import UIKit

class UserDetailCell: UITableViewCell {
    @IBOutlet var textView: TextView!
    @IBOutlet var iconView: IconView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
