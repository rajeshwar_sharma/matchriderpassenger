
import UIKit
import MapKit
import QuartzCore
class MapController: UIViewController {
    
    var start: Matchpoint?
    var destination: Matchpoint?
    var route: Route!
    var offRoute: Route!
    @IBOutlet var mapView: MKMapView!
    @IBOutlet weak var startLabel: UITextField!
    @IBOutlet weak var destinationLabel: UITextField!
    @IBOutlet weak var goButton: UIButton!
    @IBOutlet weak var destinationButton: UIButton!
    let selectionView = UIView()
    let locationManager =  CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        goButton.isEnabled = false
        goButton.alpha = 0.5
        let content = Bundle.main.loadNibNamed("MatchpointView", owner: self, options: nil)?.first as! UIView
        selectionView.frame = content.frame
        selectionView.addSubview(content)
        toolbarItems = toolbarItems! + [MKUserTrackingBarButtonItem(mapView: mapView)]
        locationManager.requestWhenInUseAuthorization()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isToolbarHidden = false
        self.mapView.add(route.polyline)
        // updateRoute()
    }
    override func viewDidAppear(_ animated: Bool) {
        updateRoute()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "timeTable":
            let navi = segue.destination as! UINavigationController
            let target = navi.topViewController as! TimeTableController
            target.route = route
        case "availableRides":
            let target = segue.destination as! AvailableRidesController
            target.route = route
            target.start = start
            target.destination = destination
        case "matchpointDetails":
            let navi = segue.destination as! UINavigationController
            let target = navi.topViewController as! MatchpointDetailController
            guard let matchpoint = mapView.selectedAnnotations.first as? Matchpoint else {
                return
            }
            target.matchpoint = matchpoint
        default:
            return
        }
    }
    
    @IBAction func toggleRoute() {
        start = nil
        destination = nil
        mapView.removeAnnotations(self.route.matchpoints)
        let route = self.route
        let offRoute = self.offRoute
        self.route = offRoute
        self.offRoute = route
        updateRoute()
        updateGoState()
    }
    
    @IBAction func showSatellite(_ sender: UIBarButtonItem) {
        if mapView.mapType == .satellite {
            sender.tintColor = UIColor.lightGray
            mapView.mapType = .standard
            return
        }
        if mapView.mapType == .standard {
            sender.tintColor = MatchRiderColor
            mapView.mapType = .satellite
            return
        }
    }
    
    @IBAction func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectStart() {
        let point = mapView.selectedAnnotations.first! as! Matchpoint
        if point == destination {
            destination = nil
        }
        start = point
        updateAnnotation(point)
    }
    
    @IBAction func selectDestination() {
        let point = mapView.selectedAnnotations.first! as! Matchpoint
        if point == start {
            start = nil
        }
        destination = point
        updateAnnotation(point)
    }
    
    @IBAction func showMatchpointDetails() {
        performSegue(withIdentifier: "matchpointDetails", sender: nil)
    }
    
    func updateRoute() {
        let title = RouteDirectionString(route.fromShortName, to: route.toShortName)
        destinationButton.setTitle(title, for: UIControlState())
        self.mapView.showAnnotations(route.matchpoints, animated: false)
    }
    
    
    func updateAnnotation(_ matchpoint: Matchpoint) {
        mapView.deselectAnnotation(matchpoint, animated: true)
        for annotation in mapView.annotations {
            guard let matchpoint = annotation as? Matchpoint else {
                continue
            }
            let view = mapView.view(for: annotation)
            view?.image = UIImage(named: "BetweenPoint")!
            view?.layer.zPosition = 1
            if matchpoint == start {
                view?.image = UIImage(named: "StartPoint")!
                view?.layer.zPosition = 2
            }
            if matchpoint == destination {
                view?.image = UIImage(named: "EndPoint")!
                view?.layer.zPosition = 2
            }
        }
        updateGoState()
    }
    
    func updateGoState() {
        if let start = start, let destination = destination {
            if destination.delay < start.delay {
                let controller = ActionControllerWrongDirection {
                    self.toggleRoute()
                }
                present(controller, animated: true, completion: nil)
            }
        }
        startLabel.text = start?.info
        destinationLabel.text = destination?.info
        if start != nil && destination != nil {
            goButton.isEnabled = true
            goButton.alpha = 1
        } else {
            goButton.isEnabled = false
            goButton.alpha = 0.5
        }
    }
}

extension MapController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        polylineRenderer.strokeColor = MatchRiderColor
        polylineRenderer.lineWidth = 4
        polylineRenderer.lineCap = .square
        return polylineRenderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let matchpoint = annotation as? Matchpoint else {
            return nil
        }
        let view = MKAnnotationView(annotation: annotation, reuseIdentifier: "Matchpoint")
        view.image = UIImage(named: "BetweenPoint")!
        if matchpoint == start {
            view.image = UIImage(named: "StartPoint")!
        }
        if matchpoint == destination {
            view.image = UIImage(named: "EndPoint")!
        }
        view.layer.zPosition = 1
        view.centerOffset = CGPoint(x: 0, y: -18)
        view.canShowCallout = true
        view.rightCalloutAccessoryView = selectionView
        return view
    }
}
