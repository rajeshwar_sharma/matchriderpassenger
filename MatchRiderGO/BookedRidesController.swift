
import UIKit
import MapKit
import CoreLocation
let textCellIdentifier = "BookedRideCell"

class BookedRidesController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var customTableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet var firstRideMessage: UILabel!
    var locationManager: CLLocationManager!
    var currentLocation : CLLocation?
    var accountButton: UIButton!
    let profileImgView = UIImageView()
    var bottomSheetView: BottomSheetView!
    var networkViewObj: NetworkView!
    var showBottomSheetCheck: Bool!
    fileprivate var bookedRides: [Ride] = []
    fileprivate var routes: [Route] = []
    fileprivate var selectedRoute: Route?
    
    override func viewDidLoad() {
        setLeftBarButton()
        showBottomSheetCheck = false
        Store.sharedStore.totalNotificationCount = 0
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            //locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        getPassengeriOSAppVersion()
        NotificationCenter.default.addObserver(self, selector: #selector(BookedRidesController.updateProfilePick(withNotification:)), name: NSNotification.Name(rawValue: "UpdateProfilePick"), object: nil)
       
//        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
//
//        statusBar.backgroundColor = UIColor.clear
       
        //redirectToCitViewController()
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        currentLocation = locations.last! as CLLocation
    }
    
    func checkNetworkAvailabilty(){
        networkViewObj = self.networkView(view: self.view)
        if networkViewObj.isNetworkAvailable! {
            setInitialValue()
        }
        networkViewObj?.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        self.view.bringSubview(toFront: networkViewObj)
    }
   
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(BookedRidesController.rideSpecificNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
        checkNetworkAvailabilty()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super.viewDidAppear(animated)
        
    }
    
    func rideSpecificNotificationReceiver(_ notification: NSNotification){
        
        checkNetworkAvailabilty()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if bottomSheetView != nil{
            bottomSheetView.removeFromSuperview()
        }
        bottomSheetView = nil
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
    }
    //************ Set Initial value *************
    func setInitialValue(){
        guard let token = Store.sharedStore.token else {
            return
        }
        registerFCMDeviceTokenToServer(token: Store.sharedStore.token!)
        
        if userDefault.object(forKey: "CityId") == nil{
            redirectToCitViewController()
        }
        else{
            let cityId = userDefault.object(forKey: "CityId") as! Int
            getAvailableRoutesByCity(token, cityId: cityId)
        }
        navigationController?.isNavigationBarHidden = false
        navigationController?.isToolbarHidden = true
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        if bookedRides.count == 0 {
            if !Store.sharedStore.firstRideDone {
                showFirstRide("Buche jetzt deine erste Fahrt!")
            }
        }
        if Store.sharedStore.cityList.count == 0{
            getCityList()
        }
        getTotalNotificationCount(token)
        requestBookedRides(token)
    }
    func updateProfilePick(withNotification notification : NSNotification) {
        setProfilePicture()
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    //*********** Set LeftBarButtonItem ************
    func setLeftBarButton()  {
        accountButton = UIButton(type: .custom)
        profileImgView.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        profileImgView.layer.cornerRadius = profileImgView.frame.width/2
        profileImgView.clipsToBounds = true
        profileImgView.contentMode = .scaleAspectFill
        profileImgView.image = UIImage(named: "Account")
        accountButton.addSubview(profileImgView)
        //accountButton.setImage( UIImage(named: "Account"), for: .normal)
        accountButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        accountButton.addTarget(self, action: #selector(BookedRidesController.accountButtonAction), for: .touchUpInside)
        let leftBarButtonItem = UIBarButtonItem(customView: accountButton)
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: false)
        leftBarButtonItem.width = -20
        setProfilePicture()
        accountButton.badgeView.position = .topRight
        accountButton.badgeView.outlineWidth = 0.0
        accountButton.badgeView.badgeColor = hexStringToUIColor(hex: "E88889")
        //accountButton.backgroundColor = .red
    }
    
    
    func accountButtonAction() {
        let navController = self.storyboard?.instantiateViewController(withIdentifier: "AccountControllerNavigation") as! UINavigationController
        self.navigationController?.present(navController, animated: true, completion: nil)
    }
    func setProfilePicture() {
        guard let token = Store.sharedStore.token else {
            return
        }
        HttpRequest.sharedRequest.accountDetails(token) { user in
            guard let user = user else {
                return
            }
            Store.sharedStore.personUserId = user.personUserId
            Store.sharedStore.personUserPhoto = user.photo
            Store.sharedStore.personName = user.firstName
            Store.sharedStore.isAdmin = user.isAdmin
            if let path = user.photo {
                
                if !path.isEmpty {
                
                let truncated = path.substring(to: path.index(before: path.endIndex))
                let exactPath = "\(truncated)l"
                HttpRequest.sharedRequest.image(exactPath, useCache: false) { image in
                    self.profileImgView.image = image
                }
            }
            }
        }
    }
    
    func getTotalNotificationCount(_ token: Token){
        HttpRequest.sharedRequest.getTotalNotificationsCount(token) { count in
            Store.sharedStore.totalNotificationCount = count
            self.accountButton.badgeView.badgeValue =  Store.sharedStore.totalNotificationCount!
            
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookedRides.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RideCell.identifier, for: indexPath) as! RideCell
        cell.selectionStyle = .none
        let ride = bookedRides[(indexPath as NSIndexPath).row]
        let name = ride.driver?.firstName
        let start = ride.start?.info
        let destination = ride.destination?.info
        let photo = HttpRequest.sharedRequest.imagesWithURL((ride.driver?.photo)!)
        cell.setStateWithDriverName(name!, start: start!, destination: destination!, date: ride.date!, image: photo)
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier! {
        case "rideDetails":
            let target = segue.destination as! DetailsController
            let row = (customTableView.indexPathForSelectedRow! as NSIndexPath).row
            target.ride = bookedRides[row]
        case "showMap":
            let navi = segue.destination as! UINavigationController
            let target = navi.topViewController as! MapController
            target.route = selectedRoute
//            target.offRoute = routes.filter { route in
//                return route.id != self.selectedRoute!.id
//                }.first!
            
            for routeTmp in routes {
                
                if selectedRoute!.startMatchPointId == routeTmp.destinationMatchPointId && selectedRoute!.destinationMatchPointId == routeTmp.startMatchPointId{
                    target.offRoute = routeTmp
                    break
                }
          }
            
            
        default:
            return
        }
    }
    
    fileprivate func getAvailableRoutesByCity(_ token: Token,cityId: Int) {
        HttpRequest.sharedRequest.getAvailableRoutesByCity(token,cityId: cityId) { routes in
            guard let routes = routes else {
                let controller = ActionControllerServerConnection {
                    self.getAvailableRoutesByCity(token,cityId: cityId)
                }
                self.present(controller, animated: true, completion: nil)
                return
            }
            self.routes = routes
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            if self.showBottomSheetCheck! {
                self.showBottomSheetCheck = false
                self.showBottomSheetView()
            }
        }
    }
    //getPassengeriOSAppVersion
    
    func getPassengeriOSAppVersion(){
        guard let token = Store.sharedStore.token else {
            return
        }
        HttpRequest.sharedRequest.getPassengeriOSAppVersion(token) { (version) in
            guard let version = version else {
                return
            }
            if  version > AppVersion{
                updateAppCheck = true
              showAlertForNewVersionUpdate()
            }
            print(version)
        }
    }
    
    
    fileprivate func requestBookedRides(_ token: Token) {
        self.view.showLoader()
        HttpRequest.sharedRequest.bookedRides(token) { rides in
            defer {
                self.customTableView.reloadData()
                self.view.hideLoader()
            }
            guard let rides = rides else {
                let error = ActionControllerServerConnection {
                    self.requestBookedRides(token)
                }
                self.view.hideLoader()
                self.present(error, animated: true, completion: nil)
                return
            }
            self.bookedRides = rides
            self.view.hideLoader()
            guard rides.count > 0 else {
                if !Store.sharedStore.firstRideDone {
                    self.showFirstRide("Buche jetzt deine erste Fahrt!")
                }
                else{
                    self.showFirstRide("Buche deine nächste Fahrt")
                }
                return
            }
            self.showTableView()
            for ride in rides {
                let path = ride.driver?.photo
                HttpRequest.sharedRequest.image(path!) { image in
                    self.customTableView.reloadData()
                }
            }
        }
    }
    
    @IBAction func bookRide() {
        

        showBottomSheetView()
  
    }
    
    func showAvailableRoots(){
        let controller = UIAlertController(title: "Fahrtrichtung", message: nil, preferredStyle: .actionSheet)
        for route in routes {
            let name = RouteDirectionString(route.fromName, to: route.toName)
            let action = UIAlertAction(title: name, style: .default) { action in
                self.selectedRoute = route
                self.performSegue(withIdentifier: "showMap", sender: nil)
            }
            controller.addAction(action)
        }
        let cancel = UIAlertAction(title: "Abbrechen", style: .cancel) { action in
            controller.dismiss(animated: true, completion: nil)
            
        }
        controller.addAction(cancel)
        present(controller, animated: true, completion: nil)
    }
    
    func redirectToCitViewController(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CityViewController") as! CityViewController
        controller.checkFirstTime = true
        //self.navigationController?.pushViewController(controller, animated: true)
        
       
        let navController = UINavigationController(rootViewController: controller)
        
        navController.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.white
        ]
        navController.navigationBar.barTintColor = MatchRiderColor
        navController.navigationBar.isTranslucent = false
        self.present(navController, animated:true, completion: nil)
    }
    func showFirstRide(_ string: String) {
        customTableView.backgroundView = Bundle.main.loadNibNamed("FirstRide", owner: self, options: nil)?.first as? UIView
        customTableView.separatorStyle = .none
        firstRideMessage.text = string
    }
    func showMessage(_ string: String) {
        customTableView.backgroundView = Bundle.main.loadNibNamed("NoRidesHome", owner: self, options: nil)?.first as? UIView
        customTableView.separatorStyle = .none
        messageLabel.text = string
    }
    func showTableView() {
        customTableView.backgroundView = nil
        customTableView.separatorStyle = .singleLine
        customTableView.reloadData()
    }
    //*********** Add bottom sheet to show Available Routes and Cities ******
    func showBottomSheetView(){
        if bottomSheetView == nil {
            bottomSheetView = Bundle.main.loadNibNamed("BottomSheetView", owner: self, options: nil)?.first as! BottomSheetView
            bottomSheetView.isHidenFlag = true
        }
        if bottomSheetView.isHidenFlag!{
            bottomSheetView.isHidenFlag = false
            bottomSheetView.frame = self.view.bounds
            self.view.addSubview(bottomSheetView)
            bottomSheetView.delegate = self
            bottomSheetView.hideFilterButton = false
            bottomSheetView.setData(routes: routes)
        }
    }
    //******** Call api to get City list *************
    func getCityList(){
        guard let token = Store.sharedStore.token else {
            return
        }
        HttpRequest.sharedRequest.getServicedCitiesList(token) { cityList in
            
            guard let cityList = cityList else {
                return
            }
            Store.sharedStore.cityList = cityList
        }
    }
}

//********** BookedRidesController Extension ***********
extension BookedRidesController: NetworkViewDelegate{
    
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
 }

extension BookedRidesController:BottomSheetViewDelegate{
    
    func filterButtonAction() {
        if bottomSheetView.isHidenFlag! {
            self.view.addSubview(bottomSheetView)
            bottomSheetView.isHidenFlag = false
            bottomSheetView.hideFilterButton = true
            bottomSheetView.setData(city: Store.sharedStore.cityList)
        }
    }
    
    func getSelected(route: Route?, city: CityModel?) {
        
        if route != nil{
            self.selectedRoute = route
            self.performSegue(withIdentifier: "showMap", sender: nil)
        } else{
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            userDefault.set(city!.cityId, forKey: "CityId")
            guard let token = Store.sharedStore.token else {
                return
            }
            self.showBottomSheetCheck = true
            getAvailableRoutesByCity(token, cityId: city!.cityId)
        }
    }
}
